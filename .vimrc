set ignorecase
set smartcase
set title
set scrolloff=3
filetype on
filetype plugin on
filetype indent on
set hlsearch
set visualbell
set nocompatible
set fileformat=unix
set backspace=indent,eol,start
